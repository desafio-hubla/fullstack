# desafio-hubla

<div style="text-align:center" align="center"><img src="https://gitlab.com/desafio-banco-inter-thales/solution/-/raw/master/public/tenor.gif" alt="money"/></div>

### This challenge was done following the ground rules present on this document [PDF](https://gitlab.com/desafio-hubla/fullstack/-/blob/main/instructions.md)

</br>

### Before project implementation It was an ideia about how could be the project, phase that I call "Conception" [documented here](https://heady-swift-7de.notion.site/Hubla-d2b8cead7a6349c9b1905d3ce92cb266)


<br/>

- The system is made with a monolith
- In next sections you gonna find the workflow of the system, they gonna be detailed ahead.

<div style="text-align:center" align="center"><img src="https://gitlab.com/desafio-hubla/fullstack/-/raw/main/public/desafio-hubla.png" alt="relacionamento"/></div>

<br/>

[Backend](https://gitlab.com/desafio-hubla/backend)
<br/>
[Frontend](https://gitlab.com/desafio-hubla/frontend)
<br/>

<hr/>

### What is needed to run the system

- Docker and Docker compose

### How to run the solution

1. Clone the repo;
2. Enter on the folder of this project;
3. Execute

```sh
docker-compose up -d
```

### Características do projeto

- It was created using Spring 2.7.6 as framework **Java**;
  - The reason for choosing Spring is that it is a familiar tool for the author.
- The `HATEOAS` level was not implemented since there is another tool that facilitates the user's navigation through the application.
- Java **v17.0.5** using **sdkman**
- The following libs were used(The motivation of each one can be much more detailed in the [document]() mentioned before):
    - [x] JUnit 5 -> Facilitates the development and execution of unit tests;
    - [x] Mockito -> Assists in the creation of mocks for unit tests;
    - [x] H2 -> It is a relatively good in-memory bank that fits the parameters established by the challenge;
    - [x] Swagger using the Open API spec -> helps describe, consume and visualize services from a REST API;
    - [x] Undertow -> Server chosen for being much more performant than tomcat;
    - [x] spring-data-jpa -> Facilitate access to the persistence layer;

- It uses gitlab-ci for;
    - Doing `automated tests`;
    - Doing code analysis with `sonarcloud` with coverage > 65%;
- `Git Flow`;
- `K8s` was not used in this project, as the author does not deem it necessary to implement this project in production;
- `docker` was used, which greatly facilitates the deployment of artifacts in large vendors that the author deems interesting in the future
- `docker-compose` docker compose was used.


## Functional Requirements

> On the [Frontend](http://localhost:8081/swagger-ui/index.html#/)

- [x] A screen (via form) to upload the file;
- [x] View the list of product transactions imported by producer/affiliate, with a totalizer of the value of transactions carried out.

> On the [Backend](http://localhost:8081/swagger-ui/index.html#/)

- [x] Parse the received file, normalize the data and store it in a relational database;

> On both

- [ ] Error handling in the backend, and report friendly error messages on the frontend.

## Not Functional Requirements

- [x] Write a README describing the project and how to set it up;
- [x] The application must be simple to configure and run, compatible with a Unix environment. You should only use free or free libraries;
- [x] Use docker for the different services that make up the application so that it works easily outside your personal environment;
- [x] Use any relational database;
   - H2

- [x] Use small commits in Git and write a nice description for each one;
- [x] Write unit tests on both the backend and frontend;
   - The author judged it not necessary on the frontend due to lack of sufficient responsibilities.

- [x] Make the code as readable and clean as possible;
- [x] Write the code (names and comments) in English. Documentation can be Portuguese if you prefer.

## Bonus Requirements

- [x] Tiver documentação das APIs do backend;
- [x] Utilizar docker-compose para orquestar os serviços num todo.
- [X] Have integration or end-to-end testing.
- [x] Have all documentation written in easy-to-understand English.
- [ ] Handle authentication and/or authorization.
